package com.atomicinteger;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class AtomicIntegerTest {

    //使用了原子integer 最后更新的值一定是50 但是不保证顺序
    private static AtomicInteger num = new AtomicInteger();

    //使用volatile修饰的num。顺序不保证，而且最后的数值不一定是50
    //private static volatile int num = 0;


    //CAS

    /**
     *
     * 数据库连接池的作用
     * https://www.cnblogs.com/ZhuChangwu/p/11150572.html
     * CAS详解
     * https://baijiahao.baidu.com/s?id=1647620168550407271
     *
     * AtomicInteger
     * https://baijiahao.baidu.com/s?id=1647621616629561468&wfr=spider&for=pc
     *
     * SpringBoot教学项目整合
     *
     * https://baijiahao.baidu.com/s?id=1692767211719567672
     *
     * @param args
     */


    public static void main(String[] args) {
        AtomicIntegerTest test = new AtomicIntegerTest();
        Thread[] threads = new Thread[5];

        for (int i = 0; i < 5; i++) {
            threads[i] = new Thread(()->{
                try {
                    for (int j = 0; j < 10; j++) {
                        System.out.println(num.incrementAndGet());
                        Thread.sleep(250);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            });
            threads[i].start();
        }
    }
}
